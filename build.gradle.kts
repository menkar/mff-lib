plugins {
    java
    `maven-publish`
}

group = "online.menkar"
version = "1.0-SNAPSHOT"

repositories {
    mavenCentral()
}

dependencies {
    // mandatory
    implementation("org.jsoup:jsoup:1.16.1")

    // unit testing
    testImplementation(platform("org.junit:junit-bom:5.9.1"))
    testImplementation("org.junit.jupiter:junit-jupiter")
}

java {
    withSourcesJar()
    withJavadocJar()
}

publishing {
    publications {
        create<MavenPublication>("mff-lib") {
            from(components["java"])
        }
    }

    repositories {
        maven {
            name = "mff-lib"
            url = uri(layout.buildDirectory.dir("repo"))
        }
    }
}

tasks.test {
    useJUnitPlatform()
}