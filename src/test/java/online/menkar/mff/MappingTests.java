package online.menkar.mff;

import online.menkar.mff.model.Metadata;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.TestInstance.Lifecycle.PER_CLASS;

@TestInstance(PER_CLASS)
public final class MappingTests {
  @Test void player() {
    assertEquals(
      Metadata.parse("https://www.mff.se/spelare/ismael-diawara/").toString(),
      "{DATE_OF_BIRTH=Fri Nov 11 00:00:00 CET 1994, PLACE_OF_BIRTH=Örebro, Sverige, HEIGHT_CENTIMETER=194, WEIGHT_KILOGRAMS=93, YOUTH_CLUB=Rynninge IK}"
    );
  }

  @Test void staff() {
    assertEquals(
      Metadata.parse("https://www.mff.se/spelare/james-slaughter/").toString(),
      "{DATE_OF_BIRTH=Thu Jul 08 00:00:00 CEST 1999, PLACE_OF_BIRTH=Tomelilla}"
    );
  }
}