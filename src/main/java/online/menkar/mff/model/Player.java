package online.menkar.mff.model;

import java.util.Map;

/**
 * This data class represents a player on the field.
 */
public final class Player extends Person {
  // number representation of this player
  private final byte number;

  // whether this player is out on loan or not
  private final boolean onLoan;

  /**
   * Instantiate a new player.
   *
   * @param number {@link Byte} the number representation of this field player.
   * @see Person#Person(String, Position, String, String, Map)
   */
  public Player(
    String name,
    Position position,
    String profileUrl,
    String purePhotoUrl,
    Map<Metadata, Object> metadata,
    int number,
    boolean outOnLoan
  ) {
    super(name, position, profileUrl, purePhotoUrl, metadata);
    this.number = (byte) number;
    this.onLoan = outOnLoan;
  }

  /**
   * @see Player#Player(String, Position, String, String, Map, int, boolean)
   */
  public Player(Person person, int number, boolean outOnLoan) {
    this(
      person.name(),
      person.position(),
      person.profileUrl(),
      person.photoUrl(),
      person.metadata(),
      number,
      outOnLoan
    );
  }

  /**
   * Get the number of this field player.
   *
   * @return {@link Integer}
   */
  public int number() {
    return this.number;
  }

  /**
   * Get whether this field player is out on loan.
   *
   * @return {@link Boolean}
   */
  public boolean isOutOnLoan() {
    return this.onLoan;
  }

  /**
   * Get a string representation of the current player.
   *
   * @return {@link String}
   */
  @Override
  public String toString() {
    return String.format(
      "{person=%s, number=%s, onLoan=%s}",
      super.toString(),
      this.number,
      this.onLoan
    );
  }
}