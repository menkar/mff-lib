package online.menkar.mff.model;

import online.menkar.mff.util.TagTranslator;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.lang.Short.parseShort;
import static java.util.Objects.requireNonNull;
import static java.util.regex.Pattern.*;

/**
 * Enumeration containing values in all relation to person and player meta.
 */
public enum Metadata implements TagTranslator {
  // birthdate of the respective person
  DATE_OF_BIRTH,
  // birthdate of the respective person
  PLACE_OF_BIRTH,
  // height of the player (bound to players only)
  HEIGHT_CENTIMETER,
  // weight of the player (bound to players only)
  WEIGHT_KILOGRAMS,
  // youth club of the player (bound to players only)
  YOUTH_CLUB;

  // value cache
  private static final Metadata[] INTERNAL = values();

  // pattern used to collect personal information from raw HTML tags
  private static final Pattern PERSONAL_INFO_PATTERN = compile(
    "(?:<[^>]*>)?([^:<]*)(?:<\\\\/[^>]*>)?",
    CASE_INSENSITIVE | MULTILINE // flags
  );

  // date format
  private static final DateFormat DATE_FORMATTER = new SimpleDateFormat("yyyy-MM-dd");

  /**
   * @see TagTranslator#matchesSwedishTag(String)
   */
  @Override
  public boolean matchesSwedishTag(String tag) {
    if (this == PLACE_OF_BIRTH)
      return tag != null && (tag.equals("födelseort") || tag.equals("födelsestad"));

    String toCheck = null;
    switch (this) {
      case DATE_OF_BIRTH:
        toCheck = "födelsedatum";
        break;

      case HEIGHT_CENTIMETER:
        toCheck = "längd";
        break;

      case WEIGHT_KILOGRAMS:
        toCheck = "vikt";
        break;

      case YOUTH_CLUB:
        toCheck = "moderklubb";
        break;
    }
    return Objects.equals(tag, toCheck);
  }

  /**
   * Attempt to fetch a metadata by its Swedish tag representation.
   *
   * @param swedishTag {@link String} the tag to search for.
   * @param defaultValue {@link Position} default metadata in case none were matching the respective tag.
   * @return {@link Metadata}
   */
  public static Metadata byTag(String swedishTag, Metadata defaultValue) {
    requireNonNull(swedishTag);
    for (final Metadata metadata : INTERNAL) {
      if (!metadata.matchesSwedishTag(swedishTag))
        continue;
      return metadata;
    }
    return defaultValue;
  }

  /**
   * @see Metadata#byTag(String, Metadata)
   */
  public static Metadata byTagOrNull(String swedishTag) {
    return byTag(swedishTag, null);
  }

  /**
   * @see Metadata#parse(Element)
   */
  public static Map<Metadata, Object> parse(String profileUrl) {
    try {
      return parse(
        Jsoup
          .connect(profileUrl)
          .get()
          .selectFirst("div.post-content")
      );
    } catch (Exception ex) {
      throw new RuntimeException(ex);
    }
  }

  /**
   * Parse the metadata from an element (assuming it is of the post-content class hierarchy).
   *
   * @param element {@link Element} the element to parse from.
   * @return {@link Map} metadata populated map extracted if successful, empty otherwise.
   */
  public static Map<Metadata, Object> parse(Element element) {
    requireNonNull(element);

    // ensure the class hierarchy is correct
    if (!element.hasClass("post-content"))
      return null;

    // collect data to be wrapped
    final Element data = element.selectFirst("div.mff-grid-columns > div.is-layout-flow > p");
    if (data == null)
      return null;

    // wrap the data
    final Map<Metadata, Object> values = new EnumMap<>(Metadata.class);
    final Matcher matcher = PERSONAL_INFO_PATTERN.matcher(data.html());

    Metadata currentMetadata = null;
    while (matcher.find()) {
      final String value = matcher.group(1).trim();
      if (value.isEmpty())
        continue;

      if (currentMetadata == null) {
        currentMetadata = byTagOrNull(value.toLowerCase());
        continue;
      }

      final Object exactValue;
      switch (currentMetadata) {
        case DATE_OF_BIRTH:
          try { exactValue = DATE_FORMATTER.parse(value); }
          catch (Exception ex) { throw new RuntimeException(ex); }
          break;
        case HEIGHT_CENTIMETER:
        case WEIGHT_KILOGRAMS:
          exactValue = parseShort(value.substring(0, value.length()-3));
          break;
        default:
          exactValue = value;
          break;
      }

      values.put(currentMetadata, exactValue);
      currentMetadata = null;
    }
    return values;
  }
}