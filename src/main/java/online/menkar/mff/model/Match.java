package online.menkar.mff.model;

import online.menkar.mff.util.Pair;

import java.time.LocalDateTime;

import static java.util.Locale.ROOT;

/**
 * This class persists of data related to a match.
 */
public class Match<Image> {
  /**
   * {@link Long} the unique identifier of this match.
   */
  public final long id;

  /**
   * {@link LocalDateTime} the kickoff date[-time] of this match.
   */
  public final LocalDateTime kickoff;

  /**
   * {@link Department} which department this match is relative to.
   */
  public final Department department;

  /**
   * {@link Type} the type of match this is.
   */
  public final Type type;

  /**
   * {@link Boolean} whether the match is or was being played at home.
   */
  public final boolean isHome;

  /**
   * {@link Pair} [name, logo (nullable)] of the match opponent.
   */
  public final Pair<String, Image> opponent;

  /**
   * {@link String} name of the venue.
   */
  public final String venue;

  /**
   * {@link String} potential (nullable) raw URL pointing towards the ticket sale for this match.
   */
  public final String ticketsUrl;

  /**
   * {@link Short} total amount of tickets sold.
   */
  public final short soldTickets;

  /**
   * {@link Short} total amount of tickets available.
   */
  public final short totalTickets;

  /**
   * Handle internal properties of the match.
   */
  public Match(long id, LocalDateTime kickoff, Department department, Type type,
               boolean isHome, Pair<String, Image> opponent, String venue,
               String ticketsUrl, short soldTickets, short totalTickets
  ) {
    this.id = id;
    this.kickoff = kickoff;
    this.department = department;
    this.type = type;
    this.isHome = isHome;
    this.opponent = opponent;
    this.venue = venue;
    this.ticketsUrl = ticketsUrl;
    this.soldTickets = soldTickets;
    this.totalTickets = totalTickets;
  }

  /**
   * Get whether this match has already been played.
   *
   * @return {@link Boolean}
   */
  public boolean hasFinished() {
    return false;
  }

  /**
   * Get whether this match is upcoming or not.
   *
   * @return {@link Boolean}
   */
  public final boolean isUpcoming() {
    return !hasFinished();
  }

  /**
   * Types of matches available.
   */
  public enum Type {
    ALLSVENSKAN,
    DIVISION_1,
    FRIENDLY,
    UNKNOWN;

    /**
     * Get the type of match by its tag definition.
     *
     * @param tag {@link String} raw tag to compare against.
     * @return {@link Type} the type found if any, or else {@link Type#UNKNOWN}.
     */
    public static Type byTag(String tag) {
      if (tag == null)
        return UNKNOWN;

      tag = tag.toLowerCase(ROOT);
      if (tag.contains("allsvenskan"))
        return ALLSVENSKAN;

      if (tag.contains("division 1"))
        return DIVISION_1;

      return tag.isEmpty() ? FRIENDLY : UNKNOWN;
    }
  }

  /**
   * Data representation of an already played {@link Match}.
   */
  public static final class Finished<Image> extends Match<Image> {
    // score of the home team
    private final byte homeScore;

    // score of the away team
    private final byte awayScore;

    /**
     * Instantiate a new, finished match.
     *
     * @param homeScore {@link Integer} final home score.
     * @param awayScore {@link Integer} final away score.
     * @see Match#Match(long, LocalDateTime, Department, Type, boolean, Pair, String, String, short, short)
     */
    public Finished(long id, LocalDateTime kickoff, Department department, Type type,
                    boolean isHome, Pair<String, Image> opponent, String venue,
                    String ticketsUrl, short soldTickets, short totalTickets,
                    int homeScore, int awayScore
    ) {
      super(id, kickoff, department, type, isHome, opponent, venue, ticketsUrl, soldTickets, totalTickets);
      this.homeScore = (byte) homeScore;
      this.awayScore = (byte) awayScore;
    }

    /**
     * @see Match#hasFinished()
     */
    @Override
    public boolean hasFinished() {
      return true;
    }

    /**
     * Get the final score of the home team.
     *
     * @return {@link Integer}
     */
    public int homeScore() {
      return this.homeScore;
    }

    /**
     * Get the final score of the away team.
     *
     * @return {@link Integer}
     */
    public int awayScore() {
      return this.awayScore;
    }
  }
}