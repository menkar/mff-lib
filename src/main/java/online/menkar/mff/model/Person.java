package online.menkar.mff.model;

import java.util.EnumMap;
import java.util.Map;
import java.util.Optional;

import static java.util.Objects.requireNonNull;
import static java.util.Optional.ofNullable;
import static online.menkar.mff.util.Helper.transformNotNull;

/**
 * This class holds general data about a person at MFF.
 */
public class Person {
  // name of this person
  private final String name;

  // position bound to this person
  private final Position position;

  // direct link to the person's respective profile
  private final String profileUrl;

  // potential photo url of this person
  private final String purePhotoUrl;

  // optional metadata of this person's information
  private final Map<Metadata, Object> metadata;

  /**
   * Instantiate a new person.
   *
   * @param name {@link String} the name of this person.
   * @param position {@link Position} the position of this person.
   * @param profileUrl {@link String} direct link to this person's profile.
   * @param purePhotoUrl {@link String} optional photo url of this person.
   * @param metadata {@link Map} optional metadata to bind.
   */
  public Person(String name, Position position, String profileUrl, String purePhotoUrl, Map<Metadata, Object> metadata) {
    this.name = requireNonNull(name);
    this.position = requireNonNull(position);
    this.profileUrl = requireNonNull(profileUrl);
    this.purePhotoUrl = purePhotoUrl;
    this.metadata = transformNotNull(metadata, m -> m.isEmpty() ? null : new EnumMap<>(m));
  }

  /**
   * Get the name of this person.
   *
   * @return {@link String}
   */
  public final String name() {
    return this.name;
  }

  /**
   * Get the position of this person.
   *
   * @return {@link Position}
   */
  public final Position position() {
    return this.position;
  }

  /**
   * Get the profile URL of this person.
   *
   * @return {@link String}
   */
  public String profileUrl() {
    return this.profileUrl;
  }

  /**
   * Get the URL pointer to this person's photo, if any, otherwise null.
   *
   * @return {@link String}
   */
  public String photoUrl() {
    return this.purePhotoUrl;
  }

  /**
   * Get whether this person has a photo url set.
   *
   * @return {@link Boolean}
   */
  public final boolean hasPhotoUrl() {
    return purePhotoUrl != null;
  }

  /**
   * Get the potential metadata bound to this person.
   *
   * @return {@link Map}
   */
  public Map<Metadata, Object> metadata() {
    return this.metadata;
  }

  /**
   * Get the value of a metadata.
   *
   * @param type {@link Metadata} the type of metadata value to fetch.
   * @return {@link Type} the value at hand, if any, or else null.
   * @param <Type> type to cast the object to.
   */
  public <Type> Type meta(Metadata type) {
    return (Type) (hasMetadata() ? metadata.get(type) : null);
  }

  /**
   * Get the potential value of a metadata.
   *
   * @param type {@link Metadata} the type of metadata value to fetch.
   * @return {@link Optional} empty if no value was found for the metadata, otherwise the value at hand.
   * @param <Type> type to cast the object to.
   * @see Person#meta(Metadata)
   */
  public <Type> Optional<Type> optionalMeta(Metadata type) {
    return ofNullable(meta(type));
  }

  /**
   * Get whether this person has their metadata assigned.
   *
   * @return {@link Boolean}
   */
  public final boolean hasMetadata() {
    return metadata != null;
  }

  /**
   * Get a string representation of the current person.
   *
   * @return {@link String}
   */
  @Override
  public String toString() {
    return String.format(
      "{name=%s, position=%s, profileUrl=%s, purePhotoUrl=%s, metadata=%s}",
      this.name,
      this.position,
      this.profileUrl,
      this.purePhotoUrl,
      this.metadata
    );
  }
}