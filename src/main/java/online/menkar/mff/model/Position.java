package online.menkar.mff.model;

import online.menkar.mff.util.TagTranslator;

import java.util.Objects;

import static java.util.Objects.requireNonNull;

/**
 * Enumeration consisting of values representing
 * all available player and leading positions.
 */
public enum Position implements TagTranslator {
  // player positions
  GOALKEEPER("målvakt"),
  DEFENDER("försvarare"),
  MIDFIELDER("mittfältare"),
  STRIKER("anfallare"),

  // staff
  COACH("tränare"),
  ASSISTANT_COACH("assisterande tränare"),
  GOALKEEPING_COACH("målvaktstränare"),
  TEAM_LEADER("lagledare"),
  RESPONSIBLE_PHYS_COACH("ansvarig fystränare"),
  PHYS_COACH("fystränare"),
  PHYSIOTHERAPIST("fysioterapeut"),
  DOCTOR("läkare"),
  ANALYST("analytiker"),
  MASSAGE_THERAPIST("massageterapeut"),
  SPORTS_PSYCHOLOGIST("idrottspsykolog"),
  MATERIAL_MANAGER("materialförvaltare"),

  // higher-ups
  SPORT_CHIEF("sportchef"),
  ASSISTANT_SPORT_CHIEF("assisterande sportchef"),
  SPORT_ASSISTANT("sportassistent"),
  TECHNICAL_CHIEF("teknisk chef"),
  CHIEF_SCOUT("chefsccout"),
  SPORT_COORDINATOR("sportkoordinator");

  // value cache
  private static final Position[] INTERNAL = values();

  // swedish tag representation of each position
  public final String swedishTag;

  /**
   * @param swedishTag {@link String} string representation of the Swedish tag translation.
   */
  Position(String swedishTag) {
    this.swedishTag = swedishTag;
  }

  /**
   * @see TagTranslator#matchesSwedishTag(String)
   */
  @Override
  public boolean matchesSwedishTag(String tag) {
    return Objects.equals(tag, swedishTag);
  }

  /**
   * Attempt to fetch a position by its Swedish tag representation.
   *
   * @param swedishTag {@link String} the tag to search for.
   * @param defaultValue {@link Position} default position in case none were matching the respective tag.
   * @return {@link Position}
   */
  public static Position byTag(String swedishTag, Position defaultValue) {
    requireNonNull(swedishTag);
    for (final Position pos : INTERNAL) {
      if (!pos.swedishTag.equals(swedishTag))
        continue;
      return pos;
    }
    return defaultValue;
  }

  /**
   * @see Position#byTag(String, Position)
   */
  public static Position byTagOrNull(String swedishTag) {
    return byTag(swedishTag, null);
  }
}