package online.menkar.mff.model;

import static java.util.Objects.requireNonNull;

/**
 * Data class representation of a seasonal table entry.
 */
public final class TableEntry {
  /**
   * {@link String} optional URL of the team logo.
   */
  public final String teamLogoUrl;

  /**
   * {@link String} name of the team in this entry.
   */
  public final String teamName;

  /**
   * {@link Byte} position on the table of this team.
   */
  public final byte position;

  /**
   * {@link Byte} total matches played.
   */
  public final byte matches;

  /**
   * {@link Byte} total wins out of N ({@link TableEntry#matches}) played.
   */
  public final byte wins;

  /**
   * {@link Byte} total draws out of N ({@link TableEntry#matches}) played.
   */
  public final byte draws;

  /**
   * {@link Byte} total losses out of N ({@link TableEntry#matches}) played.
   */
  public final byte losses;

  /**
   * {@link Short} amount of goals scored.
   */
  public final short goalsScored;

  /**
   * {@link Short} amount of goals let in.
   */
  public final short goalsConceded;

  /**
   * {@link Short} total difference.
   */
  public final short difference;

  /**
   * {@link Byte} points gained from N ({@link TableEntry#matches}) played.
   */
  public final byte points;

  /**
   * Instantiate a new table entry.
   */
  public TableEntry(String logoUrl, String name, int position, int matches,
                    int wins, int draws, int losses, int goalsScored,
                    int goalsConceded, int difference, int points) {
    this.teamLogoUrl = logoUrl;
    this.teamName = requireNonNull(name);
    this.position = (byte) position;
    this.matches = (byte) matches;
    this.wins = (byte) wins;
    this.draws = (byte) draws;
    this.losses = (byte) losses;
    this.goalsScored = (short) goalsScored;
    this.goalsConceded = (short) goalsConceded;
    this.difference = (short) difference;
    this.points = (byte) points;
  }

  /**
   * Format a string representation of this table entry.
   *
   * @return {@link String}
   * @see Object#toString()
   */
  @Override
  public String toString() {
    return String.format(
      "{logoUrl=%s, name=%s, position=%s, matches=%s, wins=%s, draws=%s, losses=%s, scored=%s, conceded=%s, difference=%s, points=%s}",
      teamLogoUrl,
      teamName,
      position,
      matches,
      wins,
      draws,
      losses,
      goalsScored,
      goalsConceded,
      difference,
      points
    );
  }
}