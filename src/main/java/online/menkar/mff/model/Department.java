package online.menkar.mff.model;

import static java.util.Locale.ROOT;

/**
 * Available team departments.
 */
public enum Department {
  MEN,
  WOMEN;

  /**
   * Get the staff URL path of the current department.
   *
   * @return {@link String}
   */
  public String staffUrlPath() {
    return cased("ledarstab-a-laget", "ledarstab-dam");
  }

  /**
   * Get the roster URL path of the current department.
   * @return
   */
  public String rosterUrlPath() {
    return cased("a-laget", "a-lag-dam");
  }

  /**
   * Get a department by its tag.
   *
   * @param tag {@link String} the tag to compare against.
   * @return {@link Department} matching department, or else null.
   */
  public static Department byTag(String tag) {
    if (tag == null)
      return null;

    tag = tag.toLowerCase(ROOT);
    if (tag.contains("dam") || tag.contains("women"))
      return WOMEN;

    return tag.contains("herr") || tag.contains("men") ? MEN : null;
  }

  /**
   * Echo the return of an argument depending on the current department.
   */
  private String cased(String men, String women) {
    switch (this) {
      case MEN:   return men;
      case WOMEN: return women;
      default:    return null;
    }
  }
}