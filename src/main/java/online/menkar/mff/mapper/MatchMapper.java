package online.menkar.mff.mapper;

import online.menkar.mff.json.JsonObject;
import online.menkar.mff.json.JsonValue;
import online.menkar.mff.model.Department;
import online.menkar.mff.model.Match;
import online.menkar.mff.util.Pair;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeFormatterBuilder;
import java.util.function.Function;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static java.time.temporal.ChronoField.*;
import static java.util.Objects.requireNonNull;
import static java.util.regex.Pattern.compile;
import static online.menkar.mff.model.Department.MEN;
import static online.menkar.mff.model.Department.WOMEN;
import static online.menkar.mff.util.Helper.*;

/**
 * The internal mapper for {@link Match} objects via their respective json objects.
 */
public final class MatchMapper implements Mapper<JsonObject, Match<?>, Function<String, ?>> {
  // The datetime formatter to use when parsing ISO with optional timestamps.
  private static final DateTimeFormatter DATE_TIME_FORMATTER =
    new DateTimeFormatterBuilder()
      .appendPattern("yyyy-MM-dd")
      .optionalStart()
      .appendLiteral('T')
      .appendPattern("HH:mm:ss")
      .optionalEnd()
      .parseDefaulting(HOUR_OF_DAY, 0)
      .parseDefaulting(MINUTE_OF_HOUR, 0)
      .parseDefaulting(SECOND_OF_MINUTE, 0)
      .toFormatter();

  // Regex to find opposing team's source via.
  private static final Pattern OPPOSING_TEAM_LOGO_PATTERN =
    compile("src=\"(https://www\\.mff\\.se/app/uploads/.+/.+\\.png)\"");

  // package-private
  MatchMapper() {}

  /**
   * @see Mapper#map(Object, Object)
   */
  @Override
  public Match<?> map(JsonObject object, Function<String, ?> imageFromUrlFunc) {
    // the json object to be mapped must not be null
    requireNonNull(object);

    // ensure this is not practice
    final JsonValue jsonId = object.get("id");
    if (jsonId == null || jsonId == JsonValue.NULL)
      return null;

    // extraction
    final JsonObject meta = transformNotNull(object.get("meta"), JsonValue::asObject);
    final String rsEventSummary = transformNotNull(meta.string("rs-event-summary", null), String::toLowerCase);
    final Match.Type matchType = Match.Type.byTag(meta.string("rs-event-description", null));

    // create appropriately
    return create(
      jsonId.asNumber().longValue(),
      parseDate(meta),
      departmentOf(rsEventSummary, meta, matchType),
      matchType,
      meta.booleanValue("match-is-home"),
      new Pair<>(
        object.string("opposing-team", "Unknown"),
        imageFromUrlFunc == null ? null : transformIf(
          object.string("opposing-team-logo-img", null),
          it -> {
            final Matcher matcher = OPPOSING_TEAM_LOGO_PATTERN.matcher(it);
            if (matcher.find()) {
              final String matched = matcher.group(1);
              return imageFromUrlFunc.apply(matched);
            }
            return null;
          },
          it -> it != null && !it.isEmpty()
        )
      ),
      ifEmpty(meta.string("rs-event-location", ""), () -> meta.string("match-venue", "")).trim(),
      meta.string("tickets-url", null),
      meta.shortValue("match-sold-tickets"),
      meta.shortValue("match-total-tickets"),
      meta.shortValue("match-home-score"),
      meta.shortValue("match-away-score"),
      meta.booleanValue("match-is-played")
    );
  }

  /**
   * Parse the date of a match.
   */
  private static LocalDateTime parseDate(JsonObject object) {
    final String timeValue = object.string("rs-event-date-time-start", "");
    return LocalDateTime.parse(
      !timeValue.isEmpty() ? timeValue : object.string("date"),
      DATE_TIME_FORMATTER
    );
  }

  /**
   * Parse the department of a match.
   */
  private static Department departmentOf(String eventSummary, JsonObject meta, Match.Type matchType) {
    if (eventSummary != null) {
      if (eventSummary.contains("dam"))
        return WOMEN;

      if (eventSummary.contains("herr"))
        return MEN;
    }

    final Department looseAttempt = Department.byTag(meta.string("team-men-women", ""));
    if (looseAttempt != null )
      return looseAttempt;

    switch (matchType) {
      case ALLSVENSKAN: return MEN;
      case DIVISION_1: return WOMEN;
    }
    return null;
  }

  /**
   * Create a new {@link Match}.
   *
   * @return {@link Match} upcoming match if 'isFinished' is true, otherwise a {@link Match.Finished}.
   */
  private static Match<Object> create(
    long id, LocalDateTime kickoff, Department department, Match.Type type,
    boolean isHome, Pair<String, Object> opponent, String venue,
    String ticketsUrl, short soldTickets, short totalTickets,
    short homeScore, short awayScore, boolean isFinished
  ) {
    return !isFinished ? new Match<>(
      id, kickoff, department, type,
      isHome, opponent, venue,
      ticketsUrl, soldTickets, totalTickets
    ) : new Match.Finished<>(
      id, kickoff, department, type,
      isHome, opponent, venue,
      ticketsUrl, soldTickets, totalTickets,
      homeScore, awayScore
    );
  }
}