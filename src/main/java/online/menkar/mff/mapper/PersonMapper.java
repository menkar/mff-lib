package online.menkar.mff.mapper;

import online.menkar.mff.model.Metadata;
import online.menkar.mff.model.Person;
import org.jsoup.nodes.Element;

import static java.util.Locale.ROOT;
import static java.util.Objects.requireNonNull;
import static online.menkar.mff.model.Position.byTagOrNull;
import static online.menkar.mff.util.Helper.transformNotNull;

/**
 * The internal mapper for {@link Person} objects via their respective Jsoup elements.
 * <p>
 * <span color="#FF2E2E"><b>NOTE:</b></span> this mapper - as displayed in {@link PersonMapper#map(Object, Object, Object)} - assumes
 * that the element passed through is a div container of the <i><b>wp-block-mff-player-display > items > item > player</b></i> class hierarchy.
 */
public final class PersonMapper implements Mapper<Element, Person, Boolean> {
  // package-private
  PersonMapper() {}

  /**
   * @see Mapper#map(Object, Object)
   */
  @Override
  public Person map(Element element, Boolean includeMetadata) {
    // the element to be mapped must not be null
    requireNonNull(element);

    // ensure the assumed class hierarchy is present
    if (!element.hasClass("player"))
      throw new IllegalArgumentException("element passed is not of the wp-block-mff-player-display > items > item > player class hierarchy");

    // collect elements
    final Element profile     = failIfNull(element.selectFirst("a"), "missing profile container");
    final Element photoSource = profile.selectFirst("img");
    final Element innerInfo   = failIfNull(profile.selectFirst("div.info > div.info-inner"), "missing inner-info container");
    final Element nameAndPos  = failIfNull(innerInfo.selectFirst("div.name-position-container"), "missing name-position-container");
    final String profileUrl   = failIfNull(profile.attr("href"), "missing profile link");

    // assemble person
    return new Person(
      failIfNull(nameAndPos.selectFirst("span.name"), "missing name").html(),
      byTagOrNull(failIfNull(nameAndPos.selectFirst("span.position"), "missing position").html().toLowerCase(ROOT)),
      profileUrl,
      transformNotNull(photoSource, e -> e.attr("src")),
      transformNotNull(includeMetadata, v -> v ? Metadata.parse(profileUrl) : null)
    );
  }
}