package online.menkar.mff.mapper;

import online.menkar.mff.model.Person;
import online.menkar.mff.model.Player;
import org.jsoup.nodes.Element;

import static java.lang.Integer.parseInt;
import static online.menkar.mff.util.Helper.transformNotNull;

/**
 * The internal mapper for {@link Player} objects via their respective Jsoup elements.
 * <p>
 * <span color="#FF2E2E"><b>NOTE:</b></span> this mapper assumes that the element passed through
 * is a div container of the <i><b>wp-block-mff-player-display > items > item > player</b></i> class hierarchy.
 */
public final class PlayerMapper implements Mapper<Element, Player, PlayerMapper.Opt> {
  // package-private
  PlayerMapper() {}

  /**
   * @see Mapper#map(Object, Object)
   */
  @Override
  public Player map(Element element, PlayerMapper.Opt opts) throws Exception {
    // no need to check for the assumed class hierarchy nor if the element is null
    // as the person-mapper call below will do both of those things for us
    final Person person = PERSON.map(element, transformNotNull(opts, o -> o.includeMetadata));

    // collect player information outside the 'person norm'
    final Element innerInfo = element.selectFirst("a > div.info > div.info-inner");
    return new Player(
      person,
      parseInt(innerInfo.selectFirst("span.number").html()),
      opts != null && opts.isOutOnLoan
    );
  }

  /**
   * Create a new {@link Player} options object.
   *
   * @param includeMetadata {@link Boolean} whether the metadata should be included.
   * @param isOnLoan {@link Boolean} whether the player is out on loan.
   * @return {@link Opt}
   */
  public static Opt createOpts(boolean includeMetadata, boolean isOnLoan) {
    return new Opt(includeMetadata, isOnLoan);
  }

  /**
   * Data class of options for the player mapper.
   */
  public static final class Opt {
    /**
     * @param includeMetadata {@link Boolean} whether the metadata should be included.
     * @param isOnLoan {@link Boolean} whether the player is out on loan.
     */
    private Opt(boolean includeMetadata, boolean isOnLoan) {
      this.includeMetadata = includeMetadata;
      this.isOutOnLoan = isOnLoan;
    }

    /**
     * {@link Boolean} whether the metadata of a person should be included in the lookup.
     */
    public final boolean includeMetadata;

    /**
     * {@link Boolean} whether the field player is out on loan.
     */
    public final boolean isOutOnLoan;
  }
}