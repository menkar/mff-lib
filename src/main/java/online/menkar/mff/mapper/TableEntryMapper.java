package online.menkar.mff.mapper;

import online.menkar.mff.model.Match;
import online.menkar.mff.model.TableEntry;
import org.jsoup.nodes.Element;

import static java.lang.Integer.parseInt;
import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static online.menkar.mff.model.Match.Type.ALLSVENSKAN;
import static online.menkar.mff.model.Match.Type.DIVISION_1;

/**
 * The internal mapper for {@link TableEntry} objects via their respective Jsoup elements.
 * <p>
 * <span color="#FF2E2E"><b>NOTE:</b></span> this mapper - as displayed in {@link TableEntryMapper#map(Object, Object, Object)} - assumes
 * that the element passed through is a 'tr' element of the <i><b>standings-table__row</b></i> (div 1) class hierarchy or <i><b>div.show__desktop > table.table > tbody > tr.normal</b></i> (allsvenskan).
 */
public final class TableEntryMapper implements Mapper<Element, TableEntry, Match.Type> {
  // package-private
  TableEntryMapper() {}

  /**
   * @see Mapper#map(Object, Object)
   */
  @Override
  public TableEntry map(Element element, Match.Type type) {
    // the element to be mapped and match type must not be null
    requireNonNull(element);
    requireNonNull(type);

    // handle accordingly
    final boolean isAllsvenskan = type == ALLSVENSKAN;
    return !isAllsvenskan && type != DIVISION_1 ? null
      : (isAllsvenskan ? allsvenskan(element) : divisionOne(element));
  }

  /**
   * Construct a table entry based on the {@link Match.Type#DIVISION_1} structure.
   *
   * @param element {@link Element} base Jsoup element of the table entry.
   * @return {@link TableEntry}
   */
  private static TableEntry divisionOne(Element element) {
    // ensure the assumed class hierarchy is present
    if (!element.hasClass("standings-table__row"))
      throw new IllegalArgumentException("element passed is not of the 'standings-table__row' class hierarchy");

    // construct
    return new TableEntry(
      element.selectFirst(divOneCell("logo", " > figure.team-logo > img.team-logo__img")).attr("data-src"),
      element.selectFirst(divOneCell("team", " > a.standings-table__cell-team-link")).html(),
      divOneCellStat(element, "position"),
      divOneCellStat(element, "games"),
      divOneCellStat(element, "wins"),
      divOneCellStat(element, "draws"),
      divOneCellStat(element, "losses"),
      divOneCellStat(element, "scored"),
      divOneCellStat(element, "conceded"),
      divOneCellStat(element, "diff"),
      divOneCellStat(element, "points")
    );
  }

  /**
   * Construct a table entry based on the {@link Match.Type#ALLSVENSKAN} structure.
   *
   * @param element {@link Element} base Jsoup element of the table entry.
   * @return {@link TableEntry}
   */
  private static TableEntry allsvenskan(Element element) {
    // ensure the assumed class hierarchy is present
    if (!element.hasClass("normal"))
      throw new IllegalArgumentException("element passed is not of the 'normal' class hierarchy");

    // general props
    final String[] goals = element.selectFirst("td:nth-child(9)").html().split("-");
    final int scored = parseInt(goals[0]);
    final int conceded = parseInt(goals[1]);

    // construct
    return new TableEntry(
      element.selectFirst("th:nth-child(3) > img").attr("src"),
      element.selectFirst("td:nth-child(4)").html(),
      allsvenskanStat(element, 1),
      allsvenskanStat(element, 5),
      allsvenskanStat(element, 6),
      allsvenskanStat(element, 7),
      allsvenskanStat(element, 8),
      scored,
      conceded,
      scored - conceded,
      parseInt(element.selectFirst("td:nth-child(10)").html())
    );
  }

  /**
   * Format the standings table-cell of {@link Match.Type#DIVISION_1}.
   */
  private static String divOneCell(String of, String extra) {
    return format("td.standings-table__cell-%s%s", of, extra);
  }

  /**
   * Parse and format a statistic of a {@link Match.Type#DIVISION_1} cell.
   */
  private static int divOneCellStat(Element element, String of) {
    String raw = element.selectFirst(divOneCell(of, "")).html();
    requireNonNull(raw);

    // positive
    final char first = raw.charAt(0);
    if (first >= '0' && first <= '9')
      return parseInt(raw);

    // handle negative (they don't use the hyphen-minus so parseInt(raw) will raise an NFE)
    final int parsed = parseInt(raw.substring(1));
    return ~parsed + 1; // flip the bits to make it negative again, then add 1 to account for the bit
  }

  /**
   * Parse and format a statistic of a {@link Match.Type#ALLSVENSKAN} cell.
   */
  private static int allsvenskanStat(Element element, int child) {
    return parseInt(element.selectFirst(format("td:nth-child(%d)", child)).html());
  }
}