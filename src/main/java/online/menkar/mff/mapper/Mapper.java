package online.menkar.mff.mapper;

import online.menkar.mff.json.JsonObject;
import online.menkar.mff.model.Match;
import online.menkar.mff.model.Person;
import online.menkar.mff.model.Player;
import online.menkar.mff.model.TableEntry;
import org.jsoup.nodes.Element;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

/**
 * Interface representation of an object mapper.
 *
 * @param <From> type to map from.
 * @param <To> type to map into.
 */
@FunctionalInterface
public interface Mapper<From, To, Options> {
  /**
   * Shared mapper for {@link Person} objects.
   */
  Mapper<Element, Person, Boolean> PERSON = new PersonMapper();

  /**
   * Shared mapper for {@link Player} objects.
   */
  Mapper<Element, Player, PlayerMapper.Opt> PLAYER = new PlayerMapper();

  /**
   * Shared mapper for {@link Match} objects.
   */
  Mapper<JsonObject, Match<?>, Function<String, ?>> MATCH = new MatchMapper();

  /**
   * Shared mapper for {@link TableEntry} objects.
   */
  Mapper<Element, TableEntry, Match.Type> TABLE_ENTRY_MAPPER = new TableEntryMapper();

  /**
   * Attempt to map the passed object into the respective type.
   *
   * @param from {@link From} object to map from.
   * @param opts {@link Options} the options to abide by during the mapping of this item.
   * @return {@link To} appropriately mapped object if any, otherwise null.
   * @throws Exception if something goes wrong during the mapping of the passed {@link From} object.
   */
  To map(From from, Options opts) throws Exception;

  /**
   * @see Mapper#map(Object, Options)
   */
  default To map(From from, Options opts, Function<Exception, To> orElse) {
    try {
      return map(from, opts);
    } catch (Exception ex) {
      return requireNonNull(orElse)
        .apply(ex);
    }
  }

  /**
   * @see Mapper#map(Object, Options, Function)
   */
  default To map(From from, Options opts, Supplier<To> defaultSupplier) {
    return map(from, opts, ignored -> requireNonNull(defaultSupplier).get());
  }

  /**
   * @see Mapper#map(Object, Options, Supplier)
   */
  default To map(From from, Options opts, To defaultValue) {
    return map(from, opts, () -> defaultValue);
  }

  /**
   * @see Mapper#map(Object, Options, Function)
   * @throws RuntimeException if the mapping fails.
   */
  default To mapOrThrow(From from, Options opts) {
    return map(from, opts, ex -> { throw new RuntimeException(ex); });
  }

  /**
   * @see Mapper#map(Object, Options, Function)
   */
  default To mapOrNull(From from, Options opts) {
    return map(from, opts, ignored -> null);
  }

  /**
   * Raise a {@link Failure} exception with a specific reason applied.
   *
   * @param reason {@link String} reasoning behind this raised failure.
   */
  default void fail(String reason) {
    requireNonNull(reason);
    throw new Failure(reason);
  }

  /**
   * Fail if the result of the predicate is true.
   * @see Mapper#fail(String)
   */
  default <Type> Type failIf(Type obj, Predicate<Type> predicate, String reason) {
    if (requireNonNull(predicate).test(obj))
      fail(reason);
    return obj;
  }

  /**
   * Fail if the condition is true.
   * @see Mapper#fail(String)
   */
  default void failIf(boolean condition, String reason) {
    if (condition)
      fail(reason);
  }

  /**
   * Fail if the object passed is null.
   * @see Mapper#failIf(boolean, String)
   */
  default <Type> Type failIfNull(Type object, String reason) {
    return failIf(object, Objects::isNull, reason);
  }

  /**
   * This exception is most likely thrown when something goes wrong inside a mapper.
   */
  final class Failure extends RuntimeException {
    private Failure(String reason) { super(reason); }
  }
}