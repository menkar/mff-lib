package online.menkar.mff.json;

/**
 * The layer of {@link JsonValue} number implementations.
 */
public abstract class JsonNumber extends JsonValue {
  /**
   * @see JsonValue#type()
   */
  @Override
  protected final JsonType type() {
    return JsonType.NUMBER;
  }

  /**
   * Get this JSON number as a byte if supported.
   *
   * @return {@link Byte}
   */
  public byte byteValue() {
    throw new UnsupportedOperationException();
  }

  /**
   * Get this JSON number as a short if supported.
   *
   * @return {@link Short}
   */
  public short shortValue() {
    throw new UnsupportedOperationException();
  }

  /**
   * Get this JSON number as an integer if supported.
   *
   * @return {@link Integer}
   */
  public int intValue() {
    throw new UnsupportedOperationException();
  }

  /**
   * Get this JSON number as a long if supported.
   *
   * @return {@link Long}
   */
  public long longValue() {
    throw new UnsupportedOperationException();
  }

  /**
   * Get this JSON number as a float if supported.
   *
   * @return {@link Float}
   */
  public float floatValue() {
    throw new UnsupportedOperationException();
  }

  /**
   * Get this JSON number as a double if supported.
   *
   * @return {@link Double}
   */
  public double doubleValue() {
    throw new UnsupportedOperationException();
  }
}