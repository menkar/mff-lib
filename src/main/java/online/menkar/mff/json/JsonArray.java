package online.menkar.mff.json;

/**
 * The layer of {@link JsonValue} array implementations.
 */
public abstract class JsonArray extends JsonValue implements Iterable<JsonValue> {
  /**
   * Attempt to get a value from this array based on its index.
   *
   * @param index {@link Integer} the index to extract a value from.
   * @return {@link JsonValue} extracted value.
   */
  public abstract JsonValue get(int index);

  /**
   * Get the size of this array.
   *
   * @return {@link Integer}
   */
  public abstract int len();

  /**
   * Get a value from this array based on its index and if it's not present, return null.
   *
   * @param index {@link Integer} the index to extract a value from.
   * @return {@link JsonValue} value extracted, or else null.
   */
  public JsonValue getOrNull(int index) {
    final int len = len();
    return index >= 0 && index <= len-1 ? get(index) : null;
  }

  /**
   * @see JsonValue#type()
   */
  @Override
  protected final JsonType type() {
    return JsonType.ARRAY;
  }
}