package online.menkar.mff.json;

public enum JsonType {
  OBJECT,
  ARRAY,
  STRING,
  BOOLEAN,
  NUMBER,
  NULL
}