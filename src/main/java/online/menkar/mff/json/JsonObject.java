package online.menkar.mff.json;

import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;
import static online.menkar.mff.json.JsonType.*;

/**
 * The layer of {@link JsonValue} object implementations.
 */
public abstract class JsonObject extends JsonValue {
  /**
   * Get a value bound to this object by its associated key.
   *
   * @param key {@link String} the key to search for.
   * @return {@link JsonValue} found JSON value associated with the key passed, if any, otherwise a possible null.
   */
  public abstract JsonValue get(String key);

  /**
   * @see JsonValue#type()
   */
  @Override
  protected final JsonType type() {
    return OBJECT;
  }

  /**
   * Get an optional JSON value from this object, if absent, the default value will be fetched from the supplier.
   *
   * @param key {@link String} the key associated with the value to get.
   * @param defaultSupplier {@link Supplier} supplier used to extract the default value from.
   * @return {@link JsonValue} value extracted, if any, otherwise default.
   */
  public JsonValue getOrDefault(String key, Supplier<JsonValue> defaultSupplier) {
    final JsonValue value = get(key);
    return value != null ? value : defaultSupplier.get();
  }

  /**
   * Get the optional JSON value of key1 and if it's absent, get the JSON value of key2.
   *
   * @param key1 {@link String} key of the value to attempt to get at first attempt.
   * @param key2 {@link String} key of the value to get if the first one is absent.
   * @return {@link JsonValue} value associated with key1 if present, otherwise potential value of key2.
   */
  public JsonValue getEither(String key1, String key2) {
    return getOrDefault(key1, () -> get(key2));
  }

  /**
   * Get a JSON value by the passed path recursively, separating it via 'separator' with an order of left-to-right.
   *
   * @param separator {@link String} the string to use for separation between keys in the path parameter. '.' is used if this is null.
   * @param path {@link String} the path to extract the JSON value from using separated keys.
   * @return {@link JsonValue} extracted value, if any, otherwise null.
   */
  public JsonValue getRecursively(String separator, String path) {
    final String[] keys = requireNonNull(path).split(separator == null ? "\\." : separator);
    final int len       = keys.length;

    JsonObject next = this;
    int index = 0;

    while (index < len) {
      final JsonValue value = next.get(keys[index++]);
      if (index != len) {
        if (!value.is(OBJECT))
          return null;

        next = value.asObject();
        continue;
      }
      return value;
    }
    return null;
  }

  /**
   * @see JsonObject#getRecursively(String, String)
   */
  public JsonValue getRecursively(String path) {
    return getRecursively(null, path);
  }

  /**
   * Get the JSON value associated with the passed key and strictly attempt to get its string representation.
   *
   * @param key {@link String} the key of the associated value to get.
   * @return {@link String} representation of the JSON value associated with the key passed.
   */
  public String string(String key) {
    return get(key).string();
  }

  /**
   * Attempt to get the string representation of a JSON value associated with the recursive path passed down.
   * If the key is absent, the default value will be returned.
   *
   * @param path {@link String} recursive path of the JSON value to fetch the string representation from.
   * @param defaultValue {@link String} default value to be used if the respective JSON value path is absent.
   * @return {@link String} string representation of the JSON value fetched, if present, otherwise the default value from params.
   */
  public String string(String path, String defaultValue) {
    return valueIf(this, path, defaultValue, v -> v.is(STRING), v -> v.stringOrDefault(defaultValue));
  }

  /**
   * Attempt to get the boolean value of a JSON value associated with the recursive path passed down.
   * If the key is absent, the default value will be returned.
   *
   * @param path {@link String} recursive path of the JSON value to fetch the boolean from.
   * @param defaultValue {@link Boolean} default value to be used if the respective JSON value path is absent.
   * @return {@link Boolean} boolean of the JSON value fetched, if present, otherwise the default value from params.
   */
  public boolean booleanValue(String path, boolean defaultValue) {
    return valueIf(this, path, defaultValue, v -> v.is(BOOLEAN), v -> v.booleanValue(defaultValue));
  }

  /**
   * Defaulting to false.
   * @see JsonObject#booleanValue(String, boolean)
   */
  public boolean booleanValue(String path) {
    return booleanValue(path, false);
  }

  /**
   * @see JsonObject#numValue(JsonObject, String, Function, Number)
   */
  public byte byteValue(String path, byte defaultValue) {
    return valueIf(this, path, defaultValue, v -> v.is(NUMBER), v -> v.asNumber().byteValue());
  }

  /**
   * Defaulting to zero.
   * @see JsonObject#byteValue(String, byte)
   */
  public byte byteValue(String path) {
    return byteValue(path, (byte) 0);
  }

  /**
   * @see JsonObject#numValue(JsonObject, String, Function, Number)
   */
  public short shortValue(String path, short defaultValue) {
    return numValue(this, path, v -> v.asNumber().shortValue(), defaultValue);
  }

  /**
   * @see JsonObject#shortValue(String, short)
   */
  public short shortValue(String path) {
    return shortValue(path, (short) 0);
  }

  /**
   * @see JsonObject#numValue(JsonObject, String, Function, Number)
   */
  public int intValue(String path, int defaultValue) {
    return numValue(this, path, v -> v.asNumber().intValue(), defaultValue);
  }

  /**
   * @see JsonObject#intValue(String, int)
   */
  public int intValue(String path) {
    return intValue(path, 0);
  }

  /**
   * @see JsonObject#numValue(JsonObject, String, Function, Number)
   */
  public long longValue(String path, long defaultValue) {
    return numValue(this, path, v -> v.asNumber().longValue(), defaultValue);
  }

  /**
   * @see JsonObject#longValue(String, long)
   */
  public long longValue(String path) {
    return longValue(path, 0);
  }

  /**
   * @see JsonObject#numValue(JsonObject, String, Function, Number)
   */
  public float floatValue(String path, float defaultValue) {
    return numValue(this, path, v -> v.asNumber().floatValue(), defaultValue);
  }

  /**
   * @see JsonObject#floatValue(String, float)
   */
  public float floatValue(String path) {
    return floatValue(path, 0);
  }

  /**
   * @see JsonObject#numValue(JsonObject, String, Function, Number)
   */
  public double doubleValue(String path, double defaultValue) {
    return numValue(this, path, v -> v.asNumber().doubleValue(), defaultValue);
  }

  /**
   * @see JsonObject#doubleValue(String, double)
   */
  public double doubleValue(String path) {
    return doubleValue(path, 0);
  }

  /**
   * Attempt to get the number value of a {@link JsonValue} associated with the recursive path passed down.
   * If the key is absent, the default value will be returned.
   *
   * @param object {@link JsonObject} the object to perform this on.
   * @param path {@link String} recursive path of the {@link JsonValue} to fetch the number value from.
   * @param collector {@link Function} the transformation function to use.
   * @param defaultValue {@link N} default value to be used if the respective {@link JsonValue} path is absent.
   * @return {@link N} number value of the {@link JsonValue} fetched, if present, otherwise the default value from params.
   */
  private static <N extends Number> N numValue(JsonObject object, String path, Function<JsonValue, N> collector, N defaultValue) {
    return valueIf(object, path, defaultValue, v -> v.is(NUMBER), collector);
  }

  /**
   * Collect a value optionally, or else a default.
   */
  private static <Type> Type valueIf(
    JsonObject object,
    String path,
    Type defaultValue,
    Predicate<JsonValue> typeValidation,
    Function<JsonValue, Type> collector
  ) {
    final JsonValue value = object.getRecursively(path);
    return value != null && typeValidation.test(value)
      ? collector.apply(value)
      : defaultValue;
  }
}