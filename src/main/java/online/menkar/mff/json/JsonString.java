package online.menkar.mff.json;

/**
 * The layer of {@link JsonValue} string implementations.
 */
public abstract class JsonString extends JsonValue {
  /**
   * Get the internal string of this {@link JsonString}.
   *
   * @return {@link String}
   */
  @Override
  public abstract String string();

  /**
   * @see JsonValue#type()
   */
  @Override
  protected final JsonType type() {
    return JsonType.STRING;
  }

  /**
   * @see JsonString#string()
   * @see Object#toString()
   */
  @Override
  public String toString() {
    return this.string();
  }
}