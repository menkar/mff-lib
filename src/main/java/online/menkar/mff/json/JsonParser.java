package online.menkar.mff.json;

import static java.nio.charset.StandardCharsets.UTF_8;
import static java.util.Objects.requireNonNull;

/**
 * Representation of a JSON parser.
 */
public interface JsonParser {
  /**
   * Parse an array of bytes into a JSON object.
   *
   * @param bytes {@link Byte} array of bytes to be parsed.
   * @return {@link JsonObject}
   */
  JsonObject parseObject(byte[] bytes);

  /**
   * Parse an array of bytes into a JSON array.
   *
   * @param bytes {@link Byte} array of bytes to be parsed.
   * @return {@link JsonArray}
   */
  JsonArray parseArray(byte[] bytes);

  /**
   * @see JsonParser#parseObject(byte[])
   */
  default JsonObject parseObject(String json) {
    return parseObject(bytes(json));
  }

  /**
   * @see JsonParser#parseArray(byte[])
   */
  default JsonArray parseArray(String json) {
    return parseArray(bytes(json));
  }

  /**
   * Parse an array of bytes into its respective JSON value (object or array).
   *
   * @param json {@link String} the JSON string content to be parsed.
   * @return {@link JsonValue} respective JSON value.
   */
  default JsonValue parse(String json) {
    if (json.isEmpty())
      return null;

    final char start = json.charAt(0);
    return start == '{' ? parseObject(json) : (start == '[' ? parseArray(json) : null);
  }

  /**
   * Parse an array of bytes into its respective JSON value (object or array) if supported.
   *
   * @param bytes {@link Byte} array of bytes to be parsed.
   * @return {@link JsonValue} respective JSON value.
   */
  default JsonValue parse(byte[] bytes) {
    throw new UnsupportedOperationException();
  }

  /**
   * Convert a raw JSON string into an array of respective bytes.
   *
   * @param json {@link String} the string of JSON content to convert.
   * @return {@link Byte} array of bytes from the respective JSON string representation.
   */
  static byte[] bytes(String json) {
    return requireNonNull(json).getBytes(UTF_8);
  }
}