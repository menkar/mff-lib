package online.menkar.mff.json;

/**
 * The layer of {@link JsonValue} boolean implementations.
 */
public abstract class JsonBoolean extends JsonValue {
  /**
   * Get the internal boolean of this JSON value.
   *
   * @see JsonValue#booleanValue()
   */
  @Override
  public abstract boolean booleanValue();

  /**
   * @see JsonValue#type()
   */
  @Override
  protected final JsonType type() {
    return JsonType.BOOLEAN;
  }
}