package online.menkar.mff.json;

import static online.menkar.mff.json.JsonType.BOOLEAN;
import static online.menkar.mff.json.JsonType.STRING;

/**
 * The base layer of possible JSON values.
 */
public abstract class JsonValue {
  /**
   * {@link JsonValue} representation of null values of JSON arrays and objects.
   */
  public static final JsonValue NULL = new JsonValue() {
    @Override
    protected JsonType type() {
      return JsonType.NULL;
    }
  };

  /**
   * No extending outside the friend-zone.
   */
  JsonValue() {}

  /**
   * Get the type of this JSON value.
   *
   * @return {@link JsonType}
   */
  protected abstract JsonType type();

  /**
   * Get whether this JSON value is of the passed type.
   *
   * @param type {@link JsonType} the JSON type to check for.
   * @return {@link Boolean} true if the types match, false otherwise.
   */
  public final boolean is(JsonType type) {
    return type == type();
  }

  /**
   * Get whether this JSON value is NULL or not.
   *
   * @return {@link Boolean} true if it matches against {@link JsonValue#NULL}, false otherwise.
   */
  public final boolean isNull() {
    return this == NULL;
  }

  /**
   * Get this value as a JSON object if supported.
   *
   * @return {@link JsonObject}
   */
  public JsonObject asObject() {
    return (JsonObject) this; // default logic
  }

  /**
   * Get this value as a JSON array if supported.
   *
   * @return {@link JsonArray}
   */
  public JsonArray asArray() {
    return (JsonArray) this; // default logic
  }

  /**
   * Get this value as a number if supported.
   *
   * @return {@link JsonNumber}
   */
  public JsonNumber asNumber() {
    return (JsonNumber) this; // default logic
  }

  /**
   * Get the string representation of this value if supported.
   * Merely here for extractions from {@link JsonString}.
   *
   * @return {@link String}
   */
  public String string() {
    throw new UnsupportedOperationException();
  }

  /**
   * Get the string representation of this value if it's a {@link JsonString},
   * otherwise use the default value passed through the params.
   *
   * @param defaultValue {@link String} the default value to be used if this value is not a {@link JsonString}.
   * @return {@link String} representation of this value if supported, otherwise default.
   */
  public String stringOrDefault(String defaultValue) {
    return is(STRING) ? string() : defaultValue;
  }

  /**
   * Get the string representation of this value if it's a {@link JsonString}, otherwise
   * fetch a different string representation from another object by its key (enforced).
   *
   * @param key {@link String} the key of the object to get extract a strict string representation from if this is not a {@link JsonString}.
   * @return {@link String} this value as a string representation if supported, otherwise the strict string of another nested object.
   */
  public String stringOrOther(String key) {
    return is(STRING) ? string() : asObject().string(key);
  }

  /**
   * Get this JSON value as a boolean if supported.
   *
   * @return {@link Boolean}
   */
  public boolean booleanValue() {
    throw new UnsupportedOperationException();
  }

  /**
   * Get this JSON value as a boolean if supported, otherwise the default value passed through params.
   *
   * @param defaultValue {@link Boolean} the default boolean value to use if this is not a {@link JsonBoolean}.
   * @return {@link Boolean} boolean representation if supported, otherwise the default value.
   */
  public boolean booleanValue(boolean defaultValue) {
    return is(BOOLEAN) ? booleanValue() : defaultValue;
  }
}