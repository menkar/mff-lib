package online.menkar.mff;

import online.menkar.mff.json.JsonArray;
import online.menkar.mff.json.JsonParser;
import online.menkar.mff.json.JsonValue;
import online.menkar.mff.mapper.Mapper;
import online.menkar.mff.mapper.PlayerMapper;
import online.menkar.mff.model.*;
import online.menkar.mff.util.HttpGetter;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;

import static java.lang.String.format;
import static java.util.Objects.requireNonNull;
import static java.util.stream.Collectors.toList;
import static online.menkar.mff.json.JsonType.OBJECT;
import static online.menkar.mff.mapper.Mapper.*;
import static online.menkar.mff.model.Department.WOMEN;
import static online.menkar.mff.model.Match.Type.ALLSVENSKAN;
import static online.menkar.mff.model.Match.Type.DIVISION_1;

/**
 * Globally shared util functionality of all MFF related operations.
 */
public final class MFF {
  /**
   * {@link HttpGetter} the default JSON http-getter implementation.
   */
  public static final HttpGetter<String> JSON_GETTER = url -> {
    final HttpURLConnection connection = (HttpURLConnection) url.openConnection();
    connection.connect();

    final StringBuilder result = new StringBuilder();
    try (final BufferedReader reader = new BufferedReader(
      new InputStreamReader(connection.getInputStream())
    )) {
      String line;
      while ((line = reader.readLine()) != null)
        result.append(line);
    }

    connection.disconnect();
    return result.toString().trim();
  };

  /**
   * No instantiation.
   */
  private MFF() {
    throw new InstantiationError();
  }

  /**
   * Query for a list of staff depending on the department.
   *
   * @param department {@link Department} the department to query staff from.
   * @param includeMetadata {@link Boolean} whether to lookup staff metadata or not.
   * @return {@link List} populated list of staff.
   */
  public static List<Person> listOfStaff(Department department, boolean includeMetadata) {
    requireNonNull(department);
    return listFromPlayerBlock(department.staffUrlPath(), PERSON, ignored -> includeMetadata);
  }

  /**
   * Query for a list of players depending on the department.
   *
   * @param department {@link Department} the department to query players from.
   * @param includeMetadata {@link Boolean} whether to lookup player metadata or not.
   * @return {@link List} populated list of players.
   */
  public static List<Player> listOfPlayers(Department department, boolean includeMetadata) {
    requireNonNull(department);
    return listFromPlayerBlock(department.rosterUrlPath(), PLAYER, isOnLoan -> PlayerMapper.createOpts(includeMetadata, isOnLoan));
  }

  /**
   * Fetch a list of upcoming and/or finished matches.
   *
   * @param parser {@link JsonParser} the JSON parser to use.
   * @param getter {@link HttpGetter} HTTP request forwarder to use for fetching the raw JSON data.
   * @param imageFunc {@link Function} function used to transform the URL of an image into its respective data type. null = ignored.
   * @param postsPerPage {@link Integer} total of posts per page to fetch.
   * @param onlyUpcoming {@link Boolean} whether to only fetch upcoming matches or not.
   * @return {@link List} a linked list populated with respective matches as per the search at hand.
   * @param <Image> type of image to use.
   */
  public static <Image> List<Match<Image>> listOfMatches(JsonParser parser,
                                                         HttpGetter<String> getter,
                                                         Function<String, Image> imageFunc,
                                                         int postsPerPage,
                                                         boolean onlyUpcoming,
                                                         Function<Exception, Match<Image>> exceptionHandle) {
    if (getter == null)
      getter = JSON_GETTER; // default

    if (postsPerPage <= 0)
      postsPerPage = 7; // default

    final String jsonContent = getter.fetchOrThrow(
      format(
        "https://www.mff.se/wp-json/mff/v1/calendar-events?posts_per_page=%d&show_only_coming_events=%s",
        postsPerPage,
        onlyUpcoming
      )
    );

    final JsonArray jsonArray = parser.parseArray(jsonContent);
    final List<Match<Image>> matches = new LinkedList<>();

    for (final JsonValue jsonValue : jsonArray) {
      final Match<Image> mapped;
      if (!jsonValue.is(OBJECT) || (mapped = (Match<Image>) MATCH.map(jsonValue.asObject(), imageFunc, exceptionHandle::apply)) == null)
        continue;
      matches.add(mapped);
    }
    return matches;
  }

  /**
   * @see MFF#listOfMatches(JsonParser, HttpGetter, Function, int, boolean, Function)
   */
  public static <Image> List<Match<Image>> listOfMatches(JsonParser parser,
                                                         Function<String, Image> imageFunc,
                                                         int postsPerPage,
                                                         boolean onlyUpcoming,
                                                         Function<Exception, Match<Image>> exceptionHandle) {
    return listOfMatches(parser, null, imageFunc, postsPerPage, onlyUpcoming, exceptionHandle);
  }

  /**
   * @see MFF#listOfMatches(JsonParser, HttpGetter, Function, int, boolean, Function)
   */
  public static <Image> List<Match<Image>> listOfMatches(JsonParser parser,
                                                         HttpGetter<String> getter,
                                                         Function<String, Image> imageFunc,
                                                         int postsPerPage,
                                                         boolean onlyUpcoming) {
    return listOfMatches(parser, getter, imageFunc, postsPerPage, onlyUpcoming, ignored -> null);
  }

  /**
   * @see MFF#listOfMatches(JsonParser, HttpGetter, Function, int, boolean, Function)
   */
  public static <Image> List<Match<Image>> listOfMatches(JsonParser parser,
                                                         Function<String, Image> imageFunc,
                                                         int postsPerPage,
                                                         boolean onlyUpcoming) {
    return listOfMatches(parser, null, imageFunc, postsPerPage, onlyUpcoming, ignored -> null);
  }

  /**
   * Fetch a list of table entries for a specific department.
   *
   * @param department {@link Department} the department to fetch the table for.
   * @return {@link List} populated list of table entries for the respective department.
   */
  public static List<TableEntry> table(Department department) {
    requireNonNull(department);
    final List<TableEntry> entries = new LinkedList<>();

    try {
      final Document document = Jsoup.connect(tableUrl(department)).get();
      final boolean isWomen = department == WOMEN;
      final Elements blocks   = document.select(
        isWomen
          ? "table.standings-table > tbody > tr.standings-table__row"
          : "div.show__desktop > table.table > tbody > tr.normal"
      );

      final Match.Type type = isWomen ? DIVISION_1 : ALLSVENSKAN;
      for (final Element block : blocks) {
        final TableEntry mapped = TABLE_ENTRY_MAPPER.mapOrNull(block, type);
        if (mapped == null)
          continue;
        entries.add(mapped);
      }
    } catch (Exception ignored) {}
    return entries;
  }

  /**
   * @see MFF#table(Department)
   */
  public static List<TableEntry> table(Department department, Comparator<TableEntry> sorter) {
    return table(department)
      .stream()
      .sorted(requireNonNull(sorter))
      .collect(toList());
  }

  /**
   * Query for a list of player-blocks to map accordingly.
   */
  private static <P extends Person, Options> List<P> listFromPlayerBlock(
    String path,
    Mapper<Element, P, Options> mapper,
    Function<Boolean, Options> optCreator
  ) {
    final List<P> population = new LinkedList<>();
    try {
      final Document document = Jsoup.connect(baseUrl(path)).get();
      final Elements blocks   = document.select("div.wp-block-mff-player-display");

      for (final Element block : blocks) {
        final Element headerElement = block.selectFirst("h2.header");
        final boolean isLoanBlock   = headerElement != null && headerElement.html().equalsIgnoreCase("utlånade spelare");
        final Options options = optCreator.apply(isLoanBlock);

        block
          .select("div.items > div.item > div.player")
          .forEach(element -> {
            final P mapped = mapper.mapOrNull(element, options);
            if (mapped != null)
              population.add(mapped);
          });
      }
    } catch (Exception ignored) {}
    return population;
  }

  /**
   * Form the base of a URL by the passed path.
   *
   * @param path {@link String} the path to append.
   * @return {@link String} formatted URL.
   */
  private static String baseUrl(String path) {
    return format("https://mff.se/%s/", path);
  }

  /**
   * Format the URL of a table depending on the department.
   *
   * @param department {@link Department} which department to collect the URL for.
   * @return {@link String} url of respective department.
   */
  private static String tableUrl(Department department) {
    return department == WOMEN
      ? "https://svenskfotboll.se/serier-cuper/tabell-och-resultat/div-1-sodra-dam-2023/101538/"
      : "https://allsvenskan.se/tabell";
  }
}