package online.menkar.mff.util;

/**
 * Data class representation of a pair.
 *
 * @param <Left> type of object to the left.
 * @param <Right> type of object to the right.
 */
public final class Pair<Left, Right> {
  /**
   * {@link Left} the first (left) object in this pair.
   */
  public final Left left;

  /**
   * {@link Right} the second (right) object in this pair.
   */
  public final Right right;

  /**
   * Instantiate a new pair of objects.
   *
   * @param left {@link Left} the left object to assign to this pair.
   * @param right {@link Right} the right object to assign to this pair.
   */
  public Pair(Left left, Right right) {
    this.left = left;
    this.right = right;
  }

  /**
   * Generate a string representation of this pair.
   *
   * @return {@link String}
   */
  @Override
  public String toString() {
    return String.format("{%s, %s}", this.left, this.right);
  }
}