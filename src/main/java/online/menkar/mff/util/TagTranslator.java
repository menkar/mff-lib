package online.menkar.mff.util;

/**
 * This interface represents a tag translation handle.
 */
public interface TagTranslator {
  /**
   * Check whether the passed string matches an existing tag in Swedish.
   *
   * @param tag {@link String} the tag to check and match against.
   * @return {@link String}
   */
  boolean matchesSwedishTag(String tag);
}