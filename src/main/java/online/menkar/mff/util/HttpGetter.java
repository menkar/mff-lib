package online.menkar.mff.util;

import java.net.URL;

/**
 * Representation of an HTTP-responsible "getter" from provided URL.
 *
 * @param <Result> type of result from the URL passed.
 */
@FunctionalInterface
public interface HttpGetter<Result> {
  /**
   * Get data from the respective URL.
   *
   * @param url {@link URL} the url to perform the GET request on.
   * @return {@link Result} appropriate result from the request at hand.
   */
  Result fetch(URL url) throws Exception;

  /**
   * @see HttpGetter#fetch(URL)
   */
  default Result fetchOrThrow(URL url) {
    try { return fetch(url); }
    catch (Exception ex) { throw new RuntimeException(ex); }
  }

  /**
   * @see HttpGetter#fetch(URL)
   */
  default Result fetch(String url) throws Exception {
    return fetch(new URL(url));
  }

  /**
   * @see HttpGetter#fetch(String)
   */
  default Result fetchOrThrow(String url) {
    try { return fetch(url); }
    catch (Exception ex) { throw new RuntimeException(ex); }
  }
}