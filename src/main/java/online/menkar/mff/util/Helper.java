package online.menkar.mff.util;

import java.util.Objects;
import java.util.function.Function;
import java.util.function.Predicate;
import java.util.function.Supplier;

import static java.util.Objects.requireNonNull;

/**
 * A utility class to help simplify and keep things keen without clutter.
 */
public final class Helper {
  /**
   * No instantiation allowed.
   */
  private Helper() {
    throw new InstantiationError();
  }

  /**
   * Match an object against a predicate and return the corresponding instance according to the results.
   *
   * @param object {@link Type} the object to test.
   * @param conditioner {@link Predicate} predicate to test on the passed object.
   * @param defaultSupplier {@link Supplier} supplier used in fetching the default value should the conditioner result be false.
   * @return {@link Type} if the conditioner result was true, then the passed object will be returned, otherwise one from the default supplier will be collected.
   * @param <Type> type of object.
   */
  public static <Type> Type matchOrDefault(
    Type object,
    Predicate<Type> conditioner,
    Supplier<Type> defaultSupplier
  ) {
    return requireNonNull(conditioner).test(object) ? object
      : (defaultSupplier == null ? null : defaultSupplier.get());
  }

  /**
   * Get the passed object if it's not null, or else call the default supplier.
   * @see Helper#matchOrDefault(Object, Predicate, Supplier)
   */
  public static <Type> Type notNullOrDefault(Type object, Supplier<Type> defaultSupplier) {
    return matchOrDefault(object, Objects::nonNull, defaultSupplier);
  }

  /**
   * Transform an object from T to R depending on the outcome of the predicate.
   *
   * @param object {@link Type} the object to possibly be transformed.
   * @param transformer {@link Function} function used to transform from {@link Type} to {@link Return}.
   * @param conditioner {@link Predicate} predicate to test against the respective object.
   * @return {@link Return} transformed object, if the conditioner was met, otherwise null.
   * @param <Type> type of object to transform from.
   * @param <Return> type of object to transform to.
   */
  public static <Type, Return> Return transformIf(
    Type object,
    Function<Type, Return> transformer,
    Predicate<Type> conditioner
  ) {
    return requireNonNull(conditioner).test(object)
      ? requireNonNull(transformer).apply(object) : null;
  }

  /**
   * Transform an object if it's not null, otherwise the return value will per automatic be null itself.
   * @see Helper#transformIf(Object, Function, Predicate)
   */
  public static <Type, Return> Return transformNotNull(
    Type object,
    Function<Type, Return> transformer
  ) {
    return transformIf(object, transformer, Objects::nonNull);
  }

  /**
   * Use the first string value passed if it's not null nor empty, otherwise get one from the supplier.
   *
   * @param value {@link String} string to check and use as a prioritised value.
   * @param ifEmpty {@link Supplier} default value supplier if the first string is either null or empty.
   * @return {@link String}
   */
  public static String ifEmpty(String value, Supplier<String> ifEmpty) {
    return value != null && !value.isEmpty() ? value : ifEmpty.get();
  }
}